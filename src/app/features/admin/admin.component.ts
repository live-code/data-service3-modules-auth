import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fb-admin',
  template: `
    <fb-admin-menu></fb-admin-menu> 
    <div pad="xl" class="pluto">admin works!</div>
    <fb-hello></fb-hello> 
    
    <form>
      <input type="text">
    </form>
  `,
  styles: [
  ]
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
