import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'fb-catalog',
  template: `
    <p>
      catalog works!
    </p>
    
    <button (click)="getData()">get data</button>
    
    PRODOTTI: {{data?.length}}
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
  ]
})
export class CatalogComponent {
  data: any

  constructor(private http: HttpClient, private cd: ChangeDetectorRef) {

  }

  getData() {

    this.http.get<any[]>('http://localhost:3000/catalog')
      .subscribe(
          res => {
            this.data = res;
            this.cd.detectChanges()
        },
      )
  }
}
