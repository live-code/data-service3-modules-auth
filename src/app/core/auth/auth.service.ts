import { Injectable } from '@angular/core';
import { Auth } from './auth';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth: Auth;

  constructor(private http: HttpClient) {
    if (this.isLogged()) {
      this.auth = JSON.parse(localStorage.getItem('auth'));
    }
  }

  login(credentials: { username: string, password: string}): Promise<Auth> {
    const params = new HttpParams()
      .set('username', credentials.username)
      .set('password', credentials.password);

    return new Promise((resolve, rej) => {
      this.http.get<Auth>(`http://localhost:3000/login`, { params })
        .subscribe(res => {
          this.auth = res;
          resolve(res);
          // localStorage.setItem('token', res.token)
          localStorage.setItem('auth', JSON.stringify(res))
        });
    });

  }

  isLogged(): boolean {
    return !!localStorage.getItem('auth')
    // return !!this.auth;
  }

  logout() {
    this.auth = null;
    localStorage.removeItem('auth')
  }

}
