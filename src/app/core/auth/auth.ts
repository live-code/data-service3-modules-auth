export interface Auth {
  token: string;
  role?: string;
  name?: string;
}
