import { Component, Input, OnInit } from '@angular/core';

/**
 * Una semplice componente per mostrare il nome utente
 */
@Component({
  selector: 'fb-hello',
  template: `
    <p>
      hello works!
    </p>
  `,
  styles: [
  ]
})
export class HelloComponent implements OnInit {
  /**
   * Il titolo del componente
   */
  @Input() title: string;

  constructor() { }

  ngOnInit(): void {
  }

}
