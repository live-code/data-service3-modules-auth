import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { catchError, delay } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let cloned = req;

    if (this.authService.isLogged()) {
      cloned = req.clone({
        setHeaders: {
          'Authentication-JWT': this.authService.auth.token
        }
      });
    }

    return next.handle(cloned)
      .pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 401:
                  // token scaduto.. ahia!!
                break;
              case 404:
              case 0:
              default:
                this.router.navigateByUrl('login')
                break;
            }
          }
          return throwError(err)
        })
      )



  }

}
