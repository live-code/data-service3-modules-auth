import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'fb-navbar',
  template: `
    <button routerLink="login" routerLinkActive="active">login</button>
    <button
      *ngIf="authService.isLogged()"
      routerLink="catalog" routerLinkActive="active">catalog</button>
    <button routerLink="admin" routerLinkActive="active">admin</button>
    <button (click)="logoutHandler()">LOGOUT</button>
    
    token is: {{authService.auth?.token}}
  `,
  styles: [`
    .active {
      background-color: orange;
    }
  `]
})
export class NavbarComponent implements OnInit {

  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  logoutHandler() {
    this.authService.logout();
    this.router.navigateByUrl('login')
  }
}
