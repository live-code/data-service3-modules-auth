import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../../core/auth/auth.service';
import { Auth } from '../../core/auth/auth';

@Component({
  selector: 'fb-login',
  template: `
    <h1 *ngIf="name">Hello {{name}}</h1>
    
    <form #f="ngForm" (submit)="login(f.value)">
      <input type="text" [ngModel] name="username">
      <input type="password" [ngModel] name="password">
      <button type="submit">LOGIN</button>
    </form>
  `,
  styles: [
  ]
})
export class LoginComponent  {
  name: string;
  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {}

  login(credentials: any) {
    this.authService.login(credentials)
      .then((res) => {
        this.name = res.name;

        setTimeout(() => {
          this.router.navigateByUrl('catalog')
        }, 500)
      });
  }
}
