import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[pad]'
})
export class PadDirective {
  @Input() pad: 'sm' | 'xl';

  @HostBinding('style.padding') get padding() {
    return this.pad === 'sm' ? '10px' : '50px';
  }
}


