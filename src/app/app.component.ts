import { Component } from '@angular/core';

@Component({
  selector: 'fb-root',
  template: `
    <div routerLinkActive="pippo">
      
    </div>
    <fb-navbar></fb-navbar>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'dataservice-auth-demo';
}
