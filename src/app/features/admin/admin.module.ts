import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AdminMenuComponent } from './components/admin-menu.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { HelpComponent } from './components/help/help.component';



@NgModule({
  declarations: [AdminComponent, AdminMenuComponent, HelpComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: 'help', component: HelpComponent },
      { path: '', component: AdminComponent },
    ])
  ]
})
export class AdminModule { }


