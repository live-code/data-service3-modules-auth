import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelloComponent } from './components/hello/hello.component';
import { BgDirective } from './components/bg.directive';
import { PadDirective } from './components/pad.directive';



@NgModule({
  declarations: [HelloComponent, BgDirective, PadDirective],
  exports: [
    HelloComponent, BgDirective, PadDirective
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
